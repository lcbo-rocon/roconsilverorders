﻿using System.Reflection.Metadata.Ecma335;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using RoconLibrary.Utility;
using RoconSilverOrders.Utility;
using RoconSilverOrders.Models;
using System.Data;
using System.Threading.Tasks;
using RoconLibrary.Domains;
using AutoMapper;
using Serilog;
using System.IO;

namespace RoconSilverOrders.Services.Implementation
{
    public class BronzeOrderService : IBronzeOrderService
    {
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;

        public BronzeOrderService(IConfigurationRoot config, IMapper mapper)
        {
            _config = config;
            _mapper = mapper;
        }

        public void processStart(OracleConnection connection)
        {
            ConnectionUtil.openConnection(connection);
            var orderStatusConfigs = getOrderStatusConfigs(connection);
            Log.Debug("==> Number of status configurations: " + orderStatusConfigs.Count);

            var fromDate = _config["fromDate"];
            var toDate = _config["toDate"];

            Log.Information($"From [{fromDate}] to [{toDate}]");

            var oraclCmd = connection.CreateCommand();
            oraclCmd.CommandText = "SELECT CO_ODNO, STATUS, LOAD_DT_TM FROM lcbo.LCO_ORDER_STATUS" +
                                    $" WHERE LOAD_DT_TM BETWEEN TO_DATE('{fromDate}', 'yyyy-mm-dd')" +
                                    $" AND TO_DATE ('{toDate}', 'yyyy-mm-dd')" +
                                    " ORDER BY CO_ODNO, STATUSSEQUENCE DESC";

            var oraclReader = oraclCmd.ExecuteReader();
            var coODNO = new HashSet<string>();

            while (oraclReader.Read())
            {
                try
                {
                    var orderNumber = oraclReader["CO_ODNO"].ToString().Trim();
                    var status = oraclReader["STATUS"].ToString();
                    // var loadDtTM = ((DateTime)oraclReader["LOAD_DT_TM"]).ToString();
                    String loadDtTM = null;

                    if (coODNO.Add(orderNumber))
                    {
                        Log.Information($"Order number order number: [{orderNumber}] added with status [{status}]");

                        var orderStatusEntity = getNextOrderStatusEntity(orderNumber);
                        // Log.Debug("OrderStatus ==> " + orderStatusEntity.OrderNumber);

                        var publishOrderStatuses = getOrderPublishStatus(orderNumber, connection);

                        foreach(OrderStatusDetail orderStatusDetail in publishOrderStatuses)
                        {
                            Log.Information($"Order status is: [{status}] and publish status is: [{orderStatusDetail.statusIdentifier}]");
                            if(status.Equals(orderStatusDetail.statusIdentifier))
                            {
                                loadDtTM = orderStatusDetail.statusCreateTime;
                            }
                        }

                        Order order = getRoconOrder(orderStatusEntity, orderStatusConfigs, publishOrderStatuses, status, loadDtTM, connection);

                        var payload = JsonConvert.SerializeObject(order);

                        sendToFile(payload, order);
                    }
                }
                catch (Exception e)
                {
                    var errorInfo = $"A general error exception happened. Message = [{e.Message}. Stacktrace is: {e.StackTrace}]";
                    Log.Error(errorInfo);
                }
            }
            oraclReader.Close();
        }

        private void sendToFile(string payload, Order order)
        {
            // Log.Information($"Payload is: \n {payload} \n");
            var jsonFileOutput = _config["jsonFileOutput"];
            // Log.Information($"jsonFileOutput is: {jsonFileOutput}");

            var filename = jsonFileOutput.Replace("replace", order.orderHeader.sorOrderNumber.ToString());
            Log.Information($"Filename is: {filename}");

            using (StreamWriter w = File.AppendText(filename))
            {
                w.WriteLine(payload);
            }
        }

        private List<OrderStatusDetail> getOrderStatusConfigs(OracleConnection connection)
        {
            ConnectionUtil.openConnection(connection);
            //Log.Debug("Inside getOrderStatusConfig");
            List<OrderStatusDetail> orderStatusConfigs = new List<OrderStatusDetail>();
            var oraclCmd = connection.CreateCommand();
            oraclCmd.CommandText = "SELECT STATUSCYCLEPATH, STATUSSEQUENCE, STATUSIDENTIFIER, STATUSDESC, CYCLEDESC FROM LCO_ORDER_STATUS_CONFIG";
            var oraclReader = oraclCmd.ExecuteReader();
            while (oraclReader.Read())
            {
                orderStatusConfigs.Add(new OrderStatusDetail
                {
                    statusPathName = ((string)oraclReader["CYCLEDESC"]).Trim(),
                    statusSequenceNo = Int32.Parse((string)oraclReader["STATUSSEQUENCE"]),
                    statusIdentifier = ((string)oraclReader["STATUSIDENTIFIER"]).Trim(),
                });
            }
            oraclReader.Close();
            return orderStatusConfigs;
        }

        private OrderStatusEntity getNextOrderStatusEntity(string orderNumber)
        {
            var orderStatusEntity = new OrderStatusEntity
            {
                OrderNumber = orderNumber
            };
            return orderStatusEntity;
        }

        private List<OrderStatusDetail> getOrderPublishStatus(string orderNumber, OracleConnection connection)
        {
            ConnectionUtil.openConnection(connection);
            var oraclCmd = new OracleCommand("GET_ORDER_PUBLISHED_STATUS", connection);
            oraclCmd.CommandType = CommandType.StoredProcedure;

            oraclCmd.Parameters.Add("LV_SORORDERNUMBER", OracleDbType.Varchar2).Value = orderNumber;
            oraclCmd.Parameters.Add("REF_ORDERPUBLISHEDSTATUS", OracleDbType.RefCursor, ParameterDirection.InputOutput);

            var oraclReader = oraclCmd.ExecuteReader();

            List<OrderStatusDetail> publishOrderStatuses = new List<OrderStatusDetail>();
            while (oraclReader.Read())
            {
                var statusFromDB = ((string)oraclReader["statusIdentifier"]).Trim();

                publishOrderStatuses.Add(new OrderStatusDetail
                {
                    statusSequenceNo = (Int32.Parse(((string)oraclReader["statusSequenceNo"]).Trim())),
                    statusIdentifier = ((string)oraclReader["statusIdentifier"]).Trim(),
                    statusCreateTime = FormatUtils.AddTimezone(((DateTime)oraclReader["statusCreateTime"]).ToString())
                });

            }
            oraclReader.Close();
            return publishOrderStatuses;
        }

        private Order getRoconOrder(OrderStatusEntity orderStatusEntity,
                                           List<OrderStatusDetail> orderStatusConfigs,
                                           List<OrderStatusDetail> publishOrderStatuses,
                                           string status, string loadDtTM,
                                           OracleConnection connection)
        {
            Task<Order> taskOrder = SQLHelper.GetOrderByIdAsync(orderStatusEntity,
                                                                 status,
                                                                 loadDtTM,
                                                                 connection,
                                                                 _mapper);
            var order = taskOrder.Result;
            string systemIdentifier = "R";
            string systemName = "R:ROCON";
            order.orderHeader.orderSourceInfo = new OrderSourceInfo();
            order.orderHeader.orderSourceInfo.systemIdentifier = systemIdentifier;
            order.orderHeader.orderSourceInfo.systemName = systemName;
            order.orderHeader.orderSourceInfo.systemOrderStatusDetail = orderStatusConfigs.ToArray();
            // order.orderHeader.publishedOrderStatus = removePublishedOrderStatus(order, publishOrderStatuses).ToArray();
            order.orderHeader.publishedOrderStatus = publishOrderStatuses.ToArray();
            order.orderHeader.lcoOrderSystem = new SystemIdentify();
            order.orderHeader.lcoOrderSystem.systemIdentifier = systemIdentifier;
            order.orderHeader.lcoOrderSystem.systemName = systemName;
            order.orderHeader.sorSystem = new SystemIdentify();
            order.orderHeader.sorSystem.systemIdentifier = systemIdentifier;
            order.orderHeader.sorSystem.systemName = systemName;

            return order;
        }

        private List<OrderStatusDetail> removePublishedOrderStatus(Order order,
                                                                    List<OrderStatusDetail> publishOrderStatuses)
        {
            if (order.orderHeader.orderStatus != null)
            {
                var statusCode = order.orderHeader.orderStatus.orderStatusCode;
                var orderStatusDetails = new List<OrderStatusDetail>();
                foreach (OrderStatusDetail orderStatusDetail in publishOrderStatuses)
                {
                    orderStatusDetails.Add(orderStatusDetail);
                    // Log.Information($"=====> Status code: [{statusCode}] and status identifier [{orderStatusDetail.statusIdentifier}]]");
                    if (statusCode.Equals(orderStatusDetail.statusIdentifier))
                    {
                        // Log.Information("==========> Breaking");
                        break;
                    }
                }
                return orderStatusDetails;
            }
            return publishOrderStatuses;
        }
    }
}
