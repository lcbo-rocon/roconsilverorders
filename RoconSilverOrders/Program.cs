﻿using System;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Threading.Tasks;
using RoconLibrary.Utility;
using Microsoft.Extensions.DependencyInjection;
using RoconSilverOrders.Services;
using Serilog;
using Serilog.Events;
using RoconSilverOrders.Utility;

namespace RoconSilverOrders
{
    class Program
    {
        static void Main(string[] args)
        {
            IServiceCollection services = new ServiceCollection();

            Startup startup = new Startup();
            startup.ConfigureServices(services);
            IServiceProvider serviceProvider = services.BuildServiceProvider();

            var config = serviceProvider.GetService<IConfigurationRoot>();

            var loggingFilePath = config["Serilog:filepath"];
            var outputTemplate = config["Serilog:outputTemplate"];
            var rollingInterval = config["Serilog:rollingInterval"];
            var fileSizeLimitBytes = Int32.Parse(config["Serilog:fileSizeLimitBytes"]);
            var minimumLevel = config["Serilog:minimumLevel"];

            var level = (LogEventLevel)Enum.Parse(typeof(LogEventLevel), minimumLevel);

            var shouldLog = Boolean.Parse(config["shouldLog"]);

            if (shouldLog)
            {
                Log.Logger = new LoggerConfiguration()
                            .MinimumLevel.Is(level)
                            .WriteTo.Console()
                            .WriteTo.File(loggingFilePath,
                                          outputTemplate: outputTemplate,
                                          fileSizeLimitBytes: fileSizeLimitBytes,
                                          rollingInterval: (RollingInterval)Enum.Parse(typeof(RollingInterval), rollingInterval),
                                          restrictedToMinimumLevel: level)
                            .CreateLogger();
            }
            else
            {
                Log.Logger = new LoggerConfiguration()
                            .MinimumLevel.Is(level)
                            .WriteTo.Console()
                            .CreateLogger();
            }

            Log.Information($"Current dir: {Directory.GetCurrentDirectory()}");

            var connection = ConnectionUtil.getConnection(config);
            var orderServices = serviceProvider.GetService<IBronzeOrderService>();

            ConnectionUtil.openConnection(connection);
            Log.Information("\n\n");
            DateHelper.DateTimeTimeZoneInfo();
            Log.Information("\n\n");
            Log.Information("Starting process: " + DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss"));

            try
            {
                orderServices.processStart(connection);
            }
            catch (Exception e)
            {
                Log.Error("Error reading in Orderservices processStart. Message = {0}", e.Message);
                var errorInfo = $"A general error exception happened. Message = [{e.Message}. Stacktrace is: {e.StackTrace}]";
            }
        }
    }
}
