#!/usr/bin/env bash

# If you do not specify versioning it will pick the higher version
# optionally version, ex: dotnet add package RoconLibray -v 1.0.0
dotnet add package RoconLibray
dotnet restore
# dotnet run